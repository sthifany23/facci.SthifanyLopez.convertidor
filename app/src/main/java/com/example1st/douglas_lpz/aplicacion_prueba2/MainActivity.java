package com.example1st.douglas_lpz.aplicacion_prueba2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    CheckBox checkBoxFC;
    CheckBox checkBoxCF;
    EditText editTextM;
    Button buttonC;
    TextView textViewR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkBoxFC = (CheckBox) findViewById(R.id.checkBoxFC);
        checkBoxCF = (CheckBox) findViewById(R.id.checkBoxCF);
        editTextM = ( EditText) findViewById(R.id.editTextM);
        buttonC = (Button) findViewById(R.id. buttonC);
        textViewR = (TextView) findViewById(R.id. textViewR);

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxFC.isChecked()){
                    float valor = Float.parseFloat(editTextM.getText().toString());
                    float resultadoC = (float) ((valor - 32)/(1.8));
                    textViewR.setText(String.valueOf(resultadoC));
                }else{
                    float valor = Float.parseFloat(editTextM.getText().toString());
                    float resultadoF = (float) ((valor * 1.8) + 32);
                    textViewR.setText(String.valueOf(resultadoF));
                }

            }
        });
    }
}
